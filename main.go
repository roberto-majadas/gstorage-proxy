package main

import (
	"gitlab.com/roberto-majadas/gstorage-proxy/cmd"
)

func main() {
	cmd.Execute()
}
