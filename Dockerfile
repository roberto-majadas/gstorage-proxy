FROM golang:alpine AS builder

WORKDIR /go/src/app
COPY main.go go.mod go.sum ./
COPY cmd ./cmd
COPY config ./config
COPY proxy ./proxy

RUN go mod vendor
RUN go build -o bin/gstorage-proxy

FROM alpine:latest
COPY --from=builder /go/src/app/bin/gstorage-proxy /usr/local/bin/
RUN chmod +x /usr/local/bin/gstorage-proxy

# Create appuser
RUN adduser -D -g '' gstorage-proxy
USER gstorage-proxy

# Default Enviroment Variables
ENV GSPROXY_PROXY_ADDRESS "0.0.0.0"
ENV GSPROXY_PROXY_PORT 8080
ENV GSPROXY_GCP_CREDENTIALS ""

CMD ["/usr/local/bin/gstorage-proxy", "run"]