# Google Storage Proxy

## Description

Google Storage Proxy is a Proxy for Google Storage. 

## Build

```bash
go mod vendor
go build -o bin/gstorage-proxy

```

## Usage

```bash
./bin/gstorage-proxy run -c gcp-service-account-credentials.json
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[Apache2](https://choosealicense.com/licenses/apache-2.0/)