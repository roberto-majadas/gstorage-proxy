module gitlab.com/roberto-majadas/gstorage-proxy

go 1.13

require (
	cloud.google.com/go v0.38.0
	github.com/googleapis/gax-go v2.0.2+incompatible // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/prometheus/client_golang v0.9.3
	github.com/spf13/cobra v0.0.6
	github.com/spf13/viper v1.4.0
	go.opencensus.io v0.22.3 // indirect
	google.golang.org/api v0.19.0
)
