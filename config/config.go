package config

import (
	"fmt"
	"github.com/spf13/viper"
	"strings"
)

func LoadConfig (cfgFile string) {
	viper.SetConfigType("yml")
	viper.AutomaticEnv()
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
	viper.SetEnvPrefix("GSPROXY")

	if cfgFile == "" {
		viper.SetConfigName(".gstorage-proxy")
		viper.AddConfigPath(".")
		viper.AddConfigPath("$HOME")
	} else {
		viper.SetConfigFile(cfgFile)
	}

	loadDefaultSettings()

	err := viper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigParseError); ok {
			panic(fmt.Errorf("unable to parse config file with error (%s)", err))
		}
	}

}

func loadDefaultSettings() {
	viper.SetDefault("proxy.address", "0.0.0.0")
	viper.SetDefault("proxy.port", 8080)
	viper.SetDefault("gcp.credentials", "")
}