package proxy

import (
	"cloud.google.com/go/storage"
	"context"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/api/option"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	client *storage.Client
	ctx    = context.Background()
	wait   time.Duration
)

type Proxy struct {
	CredentialsFile string
	Address         string
	Port            int
}

func googleStorageProxyHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	obj := client.Bucket(vars["bucket"]).Object(vars["object"])
	attr, err := obj.Attrs(ctx)

	if err != nil {
		if err == storage.ErrObjectNotExist {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			fmt.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	if attr.CacheControl != "" {
		w.Header().Add("Cache-Control", attr.CacheControl)
	}
	if attr.ContentDisposition != "" {
		w.Header().Add("Content-Disposition", attr.ContentDisposition)
	}
	if attr.ContentEncoding != "" {
		w.Header().Add("Content-Encoding", attr.ContentEncoding)
	}
	if attr.ContentLanguage != "" {
		w.Header().Add("Content-Language", attr.ContentLanguage)
	}
	if attr.ContentType != "" {
		w.Header().Add("Content-Type", attr.ContentType)
	}
	if attr.Size > 0 {
		w.Header().Add("Content-Length", attr.ContentLanguage)
	}

	objr, err := obj.NewReader(ctx)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.Copy(w, objr)
}

func (p Proxy) Run() {
	var err error

	if p.CredentialsFile != "" {
		client, err = storage.NewClient(ctx, option.WithCredentialsFile(p.CredentialsFile))
	} else {
		client, err = storage.NewClient(ctx, option.WithoutAuthentication())
	}

	if err != nil {
		log.Fatalf("Something wrong in the creation google storage client process: %v", err)
		os.Exit(1)
	}

	r := mux.NewRouter()
	srv := &http.Server{
		Addr: fmt.Sprintf("%s:%d", p.Address, p.Port),
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r, // Pass our instance of gorilla/mux in.
	}

	r.Handle("/metrics",
		instrumentHandler("metrics", handlers.CombinedLoggingHandler(os.Stdout, promhttp.Handler())))
	r.Handle("/{bucket:[0-9a-zA-Z-_.]+}/{object:.*}",
		instrumentHandler("gcs", handlers.CombinedLoggingHandler(os.Stdout, http.HandlerFunc(googleStorageProxyHandler)))).Methods("GET", "HEAD")

	go func() {
		fmt.Println(fmt.Sprintf("Listening on %s:%d", p.Address, p.Port))
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()

	c := make(chan os.Signal, 1)
	<-c

	daemon_ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()

	srv.Shutdown(daemon_ctx)

	log.Println("shutting down")
	os.Exit(0)
}
