package cmd

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"github.com/spf13/cobra"
	"gitlab.com/roberto-majadas/gstorage-proxy/config"
)

var rootCmd = &cobra.Command{
	Use:   "gstorage-proxy",
	Short: "Google Storage proxy",
	Long: ``,
}


func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().String( "config", "", "config file (default is $HOME/.gstorage-proxy.yml)")
	viper.BindPFlag("config", rootCmd.PersistentFlags().Lookup("config"))

}

func initConfig() {
	if viper.GetString("config") == "" {
		config.LoadConfig("")
	} else {
		config.LoadConfig(viper.GetString("config"))
	}
}