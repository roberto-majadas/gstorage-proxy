package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"runtime"
)

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of gstorage-proxy",
	Long:  `Print the version number of gstorage-proxy`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Go Version:", runtime.Version())
		fmt.Println("OS / Arch:", fmt.Sprintf("%s %s", runtime.GOOS, runtime.GOARCH))
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
