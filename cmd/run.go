package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/roberto-majadas/gstorage-proxy/proxy"
)

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run the reverse proxy to google storage bucket",
	Long:  `Run the reverse proxy to google storage bucket`,
	Run: func(cmd *cobra.Command, args []string) {
		p := proxy.Proxy{
			viper.GetString("gcp.credentials"),
			viper.GetString("proxy.address"),
			viper.GetInt("proxy.port"),
		}
		p.Run()
	},
}

func init() {
	runCmd.Flags().StringP("credentials", "c", "", "GCP credentials file")
	viper.BindPFlag("gcp.credentials", runCmd.Flags().Lookup("credentials"))

	runCmd.PersistentFlags().StringP("address", "a", "0.0.0.0", "Listen address for proxy")
	viper.BindPFlag("proxy.address", runCmd.PersistentFlags().Lookup("address"))

	runCmd.PersistentFlags().IntP("port", "p", 8080, "port number")
	viper.BindPFlag("proxy.port", runCmd.PersistentFlags().Lookup("port"))

	rootCmd.AddCommand(runCmd)
}
